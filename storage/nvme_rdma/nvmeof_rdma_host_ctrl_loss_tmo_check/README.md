# storage/nvme_rdma/nvmeof_rdma_host_ctrl_loss_tmo_check

Storage: nvmeof rdma host side ctrl_loss_tmo check

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
