# Xarray-idr-radixtree-test
# This is a test wrapper for linux/tools/testing/radix-tree/
# See: https://github.com/torvalds/linux/tree/master/tools/testing/radix-tree

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
